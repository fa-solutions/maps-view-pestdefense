let FAClient = null;
let notyf = null;
let apiKey = null;
let bingKey = null;
let deliveryLinesGlobal = null;
let recordId = null;

//let bingKey = "ApG0F4UDDJImvZJw-YRhOf0wTEUWQ3O-v4mx7ua0hiYOUhHB43UR3CEoCVc_zAa4";

// aHR0cDovL2xvY2FsaG9zdDo1MDAw
// aHR0cHM6Ly9mYS1zb2x1dGlvbnMuZ2l0bGFiLmlvL21hcHMtdmlldy1wZXN0ZGVmZW5zZQ==
const SERVICE = {
  name: 'FreeAgentService',
  appletId: `aHR0cHM6Ly9mYS1zb2x1dGlvbnMuZ2l0bGFiLmlvL21hcHMtdmlldy1wZXN0ZGVmZW5zZQ==`,
};

let parseLocation = (place) => place.replace(/\s+/g, '+').replace(/,/g, '%2C');

let parseWithLabel = (waypoints) => {
  let waypointsArray = [];
  waypoints.split('|').map((wayp, index) => {
    waypointsArray.push(`markers=color:blue%7Clabel:${index + 1}%7C${parseLocation(wayp)}`);
  })
  return waypointsArray.join('&');
}

let parseTime = (time) => {
     if(time.includes(AM)) {
       return `${time.split(':')[0]}:${time.split(':')[1].substring(0,2)}:00`
     } else {
       return `${parseFloat(time.split(':')[0]) + 12}:${time.split(':')[1].substring(0,2)}:00`
     }
};

let toDataURL = (url) => {
  return fetch(url).then((response) => {
    return response.blob();
  }).then(blob => {
    return URL.createObjectURL(blob);
  });
}

let getLocation = (lonLatStr) => {
  return {
    "latitude": parseFloat(lonLatStr.split(',')[0]),
    "longitude": parseFloat(lonLatStr.split(',')[1])
  }
}


async function postData(url = '', data = {}) {
  const response = await fetch(url, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data)
  });
  return response.json();
}


let download = async (url, name='Route') => {
  const a = document.createElement("a");
  a.href = await toDataURL(url);
  a.download = `${name}.png`;
  document.body.appendChild(a);
  a.click();
  document.body.removeChild(a);
}

function updateRecord(id=recordId, entity,fieldValues={}) {
    let updatePayload = {
        entity: entity,
        id: id,
        field_values: fieldValues
    }
    FAClient.updateEntity(updatePayload, (data) => {
        console.log(data);
    })
}

Date.prototype.addHours = function(h) {
    this.setTime(this.getTime() + (h*60*60*1000));
    return this;
}


function startupService() {
  notyf = new Notyf({
    duration: 20000,
    dismissible: true,
    position: {
      x: 'center',
      y: 'bottom',
    },
    types: [
      {
        type: 'info',
        className: 'info-notyf',
        icon: false,
      },
    ],
  });

  FAClient = new FAAppletClient({
    appletId: SERVICE.appletId,
  });

  apiKey = FAClient.params.key;
  bingKey = FAClient.params.bingKey;

  const iFrame = document.querySelector('#directions-iframe');
  const mapContainer = document.querySelector('#map-container');

  FAClient.on('getDirections', ({record}) => {
    console.log(record)
    const travelMode = _.get(record, `field_values[map_field3].display_value`, 'driving').toLowerCase();
    const units = _.get(record, `field_values[map_field8].display_value`, 'metric').toLowerCase();
    const mapType = _.get(record, `field_values[map_field7].display_value`, 'roadmap').toLowerCase();
    const startLocation = _.get(record, `field_values[map_field12].display_value`, '40.6183014,-73.9985763');
    const originParsed = `origin=${parseLocation(startLocation)}`;
    const endLocation = _.get(record, `field_values[map_field16].display_value`, '40.6183014,-73.9985763');
    const destinationParsed = `destination=${parseLocation(endLocation)}`;

    let existingImg = mapContainer.querySelector('img');
    if(existingImg) {
      existingImg.remove();
    }

    recordId = record.id;
    FAClient.listEntityValues(
        {
          entity: "deliveries",
          filters: [
            {
              field_name: "parent_entity_reference_id",
              operator: "includes",
              values: [recordId],
            },
          ],
        },
        (lines) => {
          console.log(lines);
          deliveryLinesGlobal = lines;

          let locationsWithOrder = {};
          let locations = [];
          lines.map((line, index) => {
              let order = _.get(line, `field_values[deliveries_field11].value`, null);
              order =  order && order !== '' ? order : index + 1;
              locationsWithOrder[order] = _.get(line, `field_values[deliveries_field2].value`, '');
            return null;
          });

            lines.map((line, index) => {
                locations.push(locationsWithOrder[index + 1])
            });

            console.log(locations)

          const waypointsParsed = `waypoints=${parseLocation(locations.join('|'))}`;
          let baseURL = "https://www.google.com/maps/embed/v1/directions?";
          iFrame.src = `${baseURL}&key=${apiKey}&${originParsed}&${destinationParsed}&${waypointsParsed}&mode=${travelMode}&units=${units}&maptype=${mapType}`;
          iFrame.style.display = "block";
          FAClient.open();
        });
  });


  FAClient.on('openDirections', (data) => {
    const travelMode = _.get(data, `record.field_values[map_field3].display_value`, 'driving').toLowerCase();
    const units = _.get(data, `record.field_values[map_field8].display_value`, 'imperial').toLowerCase();
    const mapType = _.get(data, `record.field_values[map_field7].display_value`, 'roadmap').toLowerCase();
    let originAddress = '765 West State Road 434, Suite J Longwood, FL 32750';
    const originParsed = `origin=${parseLocation(originAddress)}`;
    const destinationParsed = `destination=${parseLocation(originAddress)}`;

      recordId = data.record.id;
      FAClient.listEntityValues(
          {
              entity: "job_lines",
              filters: [
                  {
                      field_name: "parent_entity_reference_id",
                      operator: "includes",
                      values: [recordId],
                  },
              ],
          },
          (lines) => {
              console.log(lines);
              deliveryLinesGlobal = lines;


              let locations = lines.map(line => {
                  return _.get(line, `field_values[job_lines_field4].value`, '');
              });


              let waypointsParsed = `waypoints=${parseLocation(locations.join('|'))}`
              let existingImg = mapContainer.querySelector('img');
              if(existingImg) {
                  existingImg.remove();
              }

              let baseURL = "https://www.google.com/maps/embed/v1/directions?";
              iFrame.src = `${baseURL}&key=${apiKey}&${originParsed}&${destinationParsed}&${waypointsParsed}&mode=${travelMode}&units=${units}&maptype=${mapType}`;
              iFrame.style.display = "block";
              FAClient.open();
          });


  });

  FAClient.on('openMap', ({record}) => {
    const travelMode = _.get(record, `field_values[map_field3].display_value`, 'driving').toLowerCase();
    const units = _.get(record, `field_values[map_field8].display_value`, 'metric').toLowerCase();
    const mapType = _.get(record, `field_values[map_field7].display_value`, 'roadmap').toLowerCase();
    const travelDate = _.get(record, `field_values[map_field6].display_value`, '2021-05-19T07:00:00.000Z');
    const assignedTo = _.get(record, `field_values[map_field10].display_value`, 'John Smith');
    const startLocation = _.get(record, `field_values[map_field12].display_value`, '40.6183014,-73.9985763');
    const endLocation = _.get(record, `field_values[map_field16].display_value`, '40.6183014,-73.9985763');

    recordId = record.id;
    FAClient.listEntityValues(
        {
          entity: "deliveries",
          filters: [
            {
              field_name: "parent_entity_reference_id",
              operator: "includes",
              values: [recordId],
            },
          ],
        },
        (lines) => {
          console.log(lines);
          deliveryLinesGlobal = lines;

          let locations = lines.map(line => {
            return _.get(line, `field_values[deliveries_field2].value`, '');
          });

          let mapUrl = `https://maps.googleapis.com/maps/api/staticmap?center=${startLocation}&zoom=10&size=600x450&maptype=roadmap&markers=color:green%7Clabel:S%7C${startLocation}&markers=color:red%7Clabel:E%7C${endLocation}&${parseWithLabel(locations.join('|'))}&scale=2&format=PNG&&key=${apiKey}`;

          let existingImg = mapContainer.querySelector('img');
          if(existingImg) {
            existingImg.remove();
          }

          const image = document.createElement('img');
          image.src = `${mapUrl}`;
          image.width="600";
          image.height="450";
          image.addEventListener('click', async (e) => {
            await download(mapUrl, `${assignedTo}_${travelDate}`);
          })
          mapContainer.appendChild(image);

          iFrame.src = "";
          iFrame.style.display = "none";

          FAClient.open();
        });
  });


    FAClient.on('openStaticMap', ({record}) => {
        const travelMode = _.get(record, `field_values[map_field3].display_value`, 'driving').toLowerCase();
        const units = _.get(record, `field_values[map_field8].display_value`, 'metric').toLowerCase();
        const mapType = _.get(record, `field_values[map_field7].display_value`, 'roadmap').toLowerCase();
        const travelDate = _.get(record, `field_values[map_field6].display_value`, new Date().toISOString());
        const assignedTo = _.get(record, `field_values[map_field10].display_value`, 'Route');
        const startLocation = _.get(record, `field_values[map_field12].display_value`, parseLocation('765 West State Road 434, Suite J Longwood, FL 32750'));
        const endLocation = _.get(record, `field_values[map_field16].display_value`, parseLocation('765 West State Road 434, Suite J Longwood, FL 32750'));
       /*
        let locations = [
            '725 Northlake Blvd Altamonte Springs, FL 32701',
            '6226 Bear Lake Terrace Apopka, FL 32703',
            '6041 Bolling Dr Orlando, FL 32808',
            '5417 Ferrol Dr Winter Park, FL 32792'
        ];
        */
        recordId = record.id;
        FAClient.listEntityValues(
            {
                entity: "job_lines",
                filters: [
                    {
                        field_name: "parent_entity_reference_id",
                        operator: "includes",
                        values: [recordId],
                    },
                ],
            },
            (lines) => {
                console.log(lines);
                deliveryLinesGlobal = lines;


                let locations = lines.map(line => {
                    return _.get(line, `field_values[job_lines_field4].value`, '');
                });

                let mapUrl = `https://maps.googleapis.com/maps/api/staticmap?center=${startLocation}&zoom=10&size=600x450&maptype=roadmap&markers=color:green%7Clabel:S%7C${startLocation}&markers=color:red%7Clabel:E%7C${endLocation}&${parseWithLabel(locations.join('|'))}&scale=2&format=PNG&&key=${apiKey}`;


                let existingImg = mapContainer.querySelector('img');
                if(existingImg) {
                    existingImg.remove();
                }

                const image = document.createElement('img');
                image.src = `${mapUrl}`;
                image.width="600";
                image.height="450";
                image.addEventListener('click', async (e) => {
                    await download(mapUrl, `${assignedTo}_${travelDate}`);
                })
                mapContainer.appendChild(image);

                iFrame.src = "";
                iFrame.style.display = "none";

                FAClient.open();
            });
    });


  FAClient.on("optimise", ({record}) => {
    recordId = record.id;
    FAClient.listEntityValues(
        {
          entity: "deliveries",
          filters: [
            {
              field_name: "parent_entity_reference_id",
              operator: "includes",
              values: [recordId],
            },
          ],
        },
        (lines) => {
          console.log(lines);
          deliveryLinesGlobal = lines;
          generateMap('mapPng',record, lines).then(res => console.log(res)).catch(e => console.error(e));
        });
  });
}



async function generateMap(type='mapPng', record, lines) {
  const travelMode = _.get(record, `field_values[map_field3].display_value`, 'driving').toLowerCase();
  const units = _.get(record, `field_values[map_field8].display_value`, 'metric').toLowerCase();
  const mapType = _.get(record, `field_values[map_field7].display_value`, 'roadmap').toLowerCase();
  const shiftStartTime =  _.get(record, `field_values[map_field21].display_value`, '09:00:00');
  const shiftEndTime =  _.get(record, `field_values[map_field22].display_value`, '18:00:00');
  const travelDate = _.get(record, `field_values[map_field6].display_value`, '2021-05-19T07:00:00.000Z');
  const assignedTo = _.get(record, `field_values[map_field10].display_value`, 'John Smith');
  const startLocation = _.get(record, `field_values[map_field12].display_value`, '40.6183014,-73.9985763');
  const endLocation = _.get(record, `field_values[map_field16].display_value`, '40.6183014,-73.9985763');

  const shiftStartTimeParsed = `${travelDate.split('T')[0]}T${shiftStartTime}`;
  const shiftEndTimeParsed = `${travelDate.split('T')[0]}T${shiftEndTime}`;

  let itineraryItems = lines.map(line => {
    const priority = _.get(line, `field_values[deliveries_field7].value`, '1');
    const latLonLocation = _.get(line, `field_values[deliveries_field2].value`, '40.6183014,-73.9985763');
    return {
      "openingTime": shiftStartTimeParsed,
      "closingTime": shiftEndTimeParsed,
      "dwellTime": "00:00:01.00",
      "priority": parseFloat(priority),
      "location": getLocation(latLonLocation)
    }
  });



  let postDataSample = {
    "agents": [
      {
        "name": assignedTo,
        "shifts": [
          {
            "startTime": shiftStartTimeParsed,
            "startLocation": getLocation(startLocation),
            "endTime": shiftEndTimeParsed,
            "endLocation": getLocation(endLocation)
          }
        ]
      }
    ],
    itineraryItems
  };

  console.log({postDataSample, record, lines});

  postData('https://dev.virtualearth.net/REST/V1/Routes/OptimizeItinerary?key=ApG0F4UDDJImvZJw-YRhOf0wTEUWQ3O-v4mx7ua0hiYOUhHB43UR3CEoCVc_zAa4', postDataSample)
      .then(async ({ resourceSets, statusCode }) => {
        let agentItinerary = _.get(resourceSets, `[0].resources[0].agentItineraries[0].route.wayPoints`);
        let route = _.get(resourceSets, `[0].resources[0].agentItineraries[0].route`);
        let instructions = _.get(resourceSets, `[0].resources[0].agentItineraries[0].instructions`);

        let parseDuration = (duartionStr) => {
            let durationSplit = duartionStr.split(':');
            let hours = parseFloat(durationSplit[0]) * 3600;
            let min = parseFloat(durationSplit[1]) * 60;
            let sec = parseFloat(durationSplit[2]);
            return hours + min + sec;
        }

        var et = new Date(route.endTime).addHours(5);

        let fieldValues = {
            map_field23: route.totalTravelDistance / 1000,
            map_field9: parseDuration(route.totalTravelTime),
            map_field24: et.toISOString()
        }


        let sortedLines = [];

        let linesObj = {};
        deliveryLinesGlobal.map(line => {
          const latLonLocation = _.get(line, `field_values[deliveries_field2].value`);
          if(latLonLocation) {
            linesObj[latLonLocation] = line;
            updateRecord(line.id, 'deliveries', { deleted : true })
          }
        })

        let locationPins = [];
          let order = 1;
          for (let index = 0; index < instructions.length; index++) {
              let {instructionType, itineraryItem} = instructions[index];
              if (instructionType === "VisitLocation") {
                  let latLon = `${itineraryItem.location.latitude},${itineraryItem.location.longitude}`;
                  locationPins.push(latLon);
                  let travelInfo = instructions[index - 1];
                  let travelEndTime = travelInfo.endTime;
                  let travelDuration = parseDuration(travelInfo.duration);
                  let travelDistance = travelInfo.distance / 1000;
                  let package = _.get(linesObj[latLon], `field_values[deliveries_field0].value`)
                  var et = new Date(travelEndTime).addHours(5);
                  let lineFields = {
                      deliveries_field9: et,
                      deliveries_field10: travelDistance,
                      deliveries_field11: order,
                      deliveries_field12: travelDuration,
                      deliveries_field0: package,
                      parent_entity_reference_id: record.id
                  }
                  //updateRecord(linesObj[latLon].id, 'deliveries', { deleted : true })
                  //updateRecord(linesObj[latLon].id, 'deliveries',lineFields)
                  console.log(lineFields);
                  FAClient.createEntity({entity: "deliveries", field_values: lineFields}, (data) => {
                      console.log(data);
                  })

                  sortedLines.push({lines: linesObj[latLon], itineraryItem});
                  order++;
              }
          }
          updateRecord(record.id, 'map',fieldValues);
          order = 1;

        /*
            agentItinerary.map((point, index) => {
               let latLon = `${point.latitude},${point.longitude}`;
               locationPins.push(latLon);
            })
         */


        let mapUrl = `https://maps.googleapis.com/maps/api/staticmap?center=${startLocation}&zoom=10&size=600x450&maptype=roadmap&markers=color:green%7Clabel:S%7C${startLocation}&markers=color:red%7Clabel:E%7C${endLocation}&${parseWithLabel(locationPins.join('|'))}&scale=2&format=PNG&&key=${apiKey}`;

        const iFrame = document.querySelector('#directions-iframe');
        const mapContainer = document.querySelector('#map-container');
        console.log({sortedLines});
        let existingImg = mapContainer.querySelector('img');
        if(existingImg) {
          existingImg.remove();
        }

        const image = document.createElement('img');
        image.src = `${mapUrl}`;
        image.width="600";
        image.height="450";
        image.addEventListener('click', async (e) => {
          await download(mapUrl, `${assignedTo}_${travelDate}`);
        })

        mapContainer.appendChild(image);

        iFrame.src = "";
        iFrame.style.display = "none";

        FAClient.open();
      });
  return 'Ok';
}
